"""create_transfer_table

Revision ID: b7d7cdce3ae9
Revises: 3072b9f2ae9f
Create Date: 2021-12-06 20:28:30.899882

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b7d7cdce3ae9'
down_revision = '3072b9f2ae9f'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'transfer',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('employee_id', sa.String),
        sa.Column('currency', sa.String),
        sa.Column('fee', sa.Integer),
        sa.Column('gross_amount', sa.Integer),
        sa.Column('net_amount', sa.Integer),
        sa.Column('created_at', sa.String))


def downgrade():
    op.drop_table('transfer')
