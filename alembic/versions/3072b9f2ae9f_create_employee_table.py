"""create_employee_table

Revision ID: 3072b9f2ae9f
Revises: 
Create Date: 2021-12-06 20:23:12.825229

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3072b9f2ae9f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'employee',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('company_id', sa.String),
        sa.Column('employee_id', sa.String),
        sa.Column('full_name', sa.String),
        sa.Column('current_state', sa.String),
        sa.Column('employee_code', sa.String))


def downgrade():
    op.drop_table('employee')
