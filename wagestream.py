import csv, psycopg2, re

from api import wagestream_api
from api import wagestream_db
from itertools import groupby


_reports = [
    wagestream_db.generate_employee_report,
    wagestream_db.generate_filtered_employee_report,
    wagestream_db.generate_last_net_transfer_report,
]


def malformed_employees(bearer_token: str, use_swift_code: bool = False) -> None:
    pattern = re.compile("[0-9A-Za-z]{8,11}") if use_swift_code else re.compile("[0-9]{6}")
    for emp in wagestream_api.get_employees(bearer_token):
        sort_code = emp.properties.payment_sort_code
        if not pattern.match(sort_code):
            print(f"name: {emp.full_name}, payment_sort_code: {sort_code}")


def process_employee_transfers(bearer_token: str) -> None:
    employee_names = {e.employee_id: e.full_name for e in wagestream_api.get_employees(bearer_token)}
    transfers = groupby(wagestream_api.get_transfers(bearer_token), lambda t: t.employee_id)
    with open("reports/employee_transfers.csv", "w") as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["employee_name", "amount_available", "max_amount"])
        for emp_id, transfers in transfers:
            transfers_for_emp = list(transfers)
            employee_name = employee_names[emp_id]
            max_transfer = max([t.gross_amount for t in transfers_for_emp])
            amount_available = transfers_for_emp[-1].balance_before.available_to_transfer
            writer.writerow([employee_name, amount_available, max_transfer])
    print("Report saved to reports/employee_transfers.csv")


if __name__ == "__main__":
    print("Retrieving Bearer token for API requests")
    token = wagestream_api.get_bearer_token()
    print(f"Bearer {token}")
    print("Finding malformed employees")
    malformed_employees(token)
    print("Finding malformed employees [Check for swift codes]")
    malformed_employees(token, use_swift_code=True)
    print("Processing employee transfers")
    process_employee_transfers(token)
    print("Populating database with records to generate HTML reports")
    wagestream_db.populate_employees(token)
    wagestream_db.populate_transfers(token)
    print("Generating reports from database")
    for rpt in _reports:
        rpt()
    print("DONE!!")
