import yaml


class WagestreamConfig(object):
    __CONFIG_FILE_PATH__ = "config/config.yml"

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(WagestreamConfig, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self._config_file = None

    def get_base_url(self) -> str:
        _config = self.get_config()
        return _config["wagestream"]["api"]["url"]

    def get_database_url(self) -> str:
        _config = self.get_config()
        return _config["wagestream"]["database"]["url"]

    def get_config(self) -> dict:
        if not self._config_file:
            self._config_file = yaml.load(open(self.__CONFIG_FILE_PATH__), Loader=yaml.FullLoader)
        return self._config_file
