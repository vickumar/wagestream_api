from dataclasses import dataclass
from typing import Optional


@dataclass
class Property:
    payment_sort_code: str = ""


@dataclass
class Employee:
    company_id: str
    employee_id: str
    full_name: str
    current_state: str
    default_wagerate: int
    employee_code: str
    properties: Property


@dataclass
class BalanceBefore:
    accrual_type: int
    available_from_current_pay_period: int
    available_from_previous_pay_period: int
    available_to_transfer: int
    balance_updated: bool
    days_until_payday: int


@dataclass
class Transfer:
    assigned_to_current_period: int
    balance_before: BalanceBefore
    employee_id: str
    currency: str
    fee: int
    gross_amount: int
    net_amount: int
    created_at: Optional[str]
    initiated_at: Optional[str]
    updated_at: Optional[str]
