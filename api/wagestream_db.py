import psycopg2
import pandas as pd

from api import wagestream_api
from config.config import WagestreamConfig


def populate_employees(bearer_token: str) -> None:
    print("Populating employees")
    employees = wagestream_api.get_employees(bearer_token)
    conn = psycopg2.connect(WagestreamConfig().get_database_url())
    all_employees = []
    curr_employee = 1
    for e in employees:
        all_employees.append(
            {"id": curr_employee, "company_id": e.company_id, "employee_id": e.employee_id,
             "full_name": e.full_name, "current_state": e.current_state,
             "employee_code": e.employee_code
             })
        curr_employee += 1

    with conn.cursor() as cursor:
        cursor.executemany("""
            INSERT INTO employee VALUES (
                %(id)s,
                %(company_id)s,
                %(employee_id)s,
                %(full_name)s,
                %(current_state)s,
                %(employee_code)s
            );
            """, all_employees)
    conn.commit()


def _last_net_transfer_sql() -> str:
    return("""
        select e.full_name, case when t.net_amount is null then 0 else t.net_amount end from employee e 
        left join (
            select employee_id, max(id) as transfer_id
            from transfer
            group by employee_id) max_transfer 
        left join transfer t on max_transfer.transfer_id = t.id
        on e.employee_id = max_transfer.employee_id
        order by e.full_name
    """)


def _employee_report_sql(sql_filter: str = "") -> str:
    return("""
        select e.full_name, t.* from employee e 
        inner join (
            select employee_id, count(id) as total_transfers, sum(gross_amount) as total_transfer_amount,
            avg(gross_amount) as avg_transfer_amount, sum(fee) as total_fees
            from transfer 
            group by employee_id {0}) t 
        on e.employee_id = t.employee_id
    """.format(sql_filter))


def _generate_html(table_name: str, sql_table: str) -> str:
    return("""
        <html>
        <head>{0}</head>
        <body><h3>{1}</h3><br/><br/>{2}</body>
        </html>
    """.format(table_name, table_name, sql_table))


def populate_transfers(bearer_token: str) -> None:
    print("Populating transfers")
    transfers = wagestream_api.get_transfers(bearer_token)
    conn = psycopg2.connect(WagestreamConfig().get_database_url())
    all_transfers = []
    curr_transfer = 1
    for t in transfers:
        all_transfers.append(
            {"id": curr_transfer, "employee_id": t.employee_id, "currency": t.currency,
             "fee": t.fee, "gross_amount": t.gross_amount, "net_amount": t.net_amount,
             "created_at": t.created_at
             })
        curr_transfer += 1

    with conn.cursor() as cursor:
        cursor.executemany("""
                INSERT INTO transfer VALUES (
                    %(id)s,
                    %(employee_id)s,
                    %(currency)s,
                    %(fee)s,
                    %(gross_amount)s,
                    %(net_amount)s,
                    %(created_at)s
                );
                """, all_transfers)
    conn.commit()


def generate_employee_report() -> None:
    print("Generating employee report")
    conn = psycopg2.connect(WagestreamConfig().get_database_url())
    frame = pd.read_sql_query(_employee_report_sql(), conn)
    report_html = _generate_html("Employee Report", frame.to_html())
    with open("reports/employee_report.html", "w") as html_file:
        html_file.write(report_html)
    print("Report saved to reports/employee_report.html")


def generate_filtered_employee_report() -> None:
    print("Generating filtered employee report")
    conn = psycopg2.connect(WagestreamConfig().get_database_url())
    frame = pd.read_sql_query(_employee_report_sql("having count(id) <= 3"), conn)
    report_html = _generate_html("Employee Report (Filtered, <= 3 transfers)", frame.to_html())
    with open("reports/employee_report_filtered.html", "w") as html_file:
        html_file.write(report_html)
    print("Report saved to reports/employee_report_filtered.html")


def generate_last_net_transfer_report() -> None:
    print("Generating last net transfer report")
    conn = psycopg2.connect(WagestreamConfig().get_database_url())
    frame = pd.read_sql_query(_last_net_transfer_sql(), conn)
    report_html = _generate_html("Last Net Transfer", frame.to_html())
    with open("reports/last_net_transfer_report.html", "w") as html_file:
        html_file.write(report_html)
    print("Report saved to reports/last_net_transfer_report.html")
