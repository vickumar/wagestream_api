import functools, httpx

from config.config import WagestreamConfig
from dacite import from_dict
from models.models import Employee, Transfer
from typing import List


def get_bearer_token() -> str:
    wagestream_config = WagestreamConfig().get_config()
    user = wagestream_config["wagestream"]["api"]["username"]
    pw = wagestream_config["wagestream"]["api"]["password"]
    form_data = {"username": user, "password": pw, "requires_admin": "false"}
    response = httpx.post(f"{WagestreamConfig().get_base_url()}/auth/login", data=form_data)
    return response.json()["access_token"]


def _make_authorization_header(bearer_token: str) -> dict:
    return {"Authorization": f"Bearer {bearer_token}"}


@functools.lru_cache(maxsize=None)
def get_employees(bearer_token: str) -> List[Employee]:
    with httpx.Client() as client:
        employees_url = f"{WagestreamConfig().get_base_url()}/employees/"
        response = client.get(employees_url, headers=_make_authorization_header(bearer_token))
    return [
        from_dict(data_class=Employee, data=employee_data) for employee_data in response.json()["data"]
    ]


@functools.lru_cache(maxsize=None)
def get_transfers(bearer_token: str) -> List[Transfer]:
    with httpx.Client() as client:
        transfers_url = f"{WagestreamConfig().get_base_url()}/transfers/"
        response = client.get(transfers_url, headers=_make_authorization_header(bearer_token))
    return [
        from_dict(data_class=Transfer, data=transfer_data) for transfer_data in response.json()["data"]
    ]
