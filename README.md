## Local Setup

- Install [Docker](https://docs.docker.com/get-docker/)
- Install [Docker compose](https://docs.docker.com/compose/install/)
- Build the project - run `docker-compose build`
- From the root of the project, run: `docker-compose up`


## Reports

  Reports will be generated in the `/reports` folder.  To clear them out,
  please run `rm reports/*`